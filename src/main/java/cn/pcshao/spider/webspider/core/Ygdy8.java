package cn.pcshao.spider.webspider.core;

import cn.pcshao.spider.webspider.dao.impl.NormalDao;
import cn.pcshao.spider.webspider.entity.YgdyEntity;
import cn.pcshao.spider.webspider.utils.WinSysUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.List;

/**
 * @author pcshao.cn
 * @date 2021/4/4
 **/
public class Ygdy8 {

    private NormalDao dao = new NormalDao();
    private int TIME_RIGHTCLICK_MOVE = 100;
    private int TIME_RIGHTCLICK = 500;

    public void execute(WebDriver driver) {
        String domain = "https://www.ygdy8.com";
        String nextPageUrl = domain + "/html/gndy/dyzz/index.html";
        int need = 0;

        ChromeDriver chromeDriver = new ChromeDriver();
        do {
            driver.get(nextPageUrl);
            // 获取待爬取列表
            List<WebElement> ulink = driver.findElements(By.className("ulink"));

            for (int i = need; i < ulink.size(); i++) {
                WebElement element = ulink.get(i);
                YgdyEntity entity = new YgdyEntity(element.getText());
                String href = element.getAttribute("href");
                entity.setPageLink(href);

                // 开启新窗口打开单条记录
                if (!openAndCopy(chromeDriver, href, entity))
                    continue;

                // 写入
                need = 0;
                System.out.println(nextPageUrl + "--No." + (i+1) + "-->" + entity);
                dao.add(entity);
                // dao.update(entity);
            }

            WebElement nextPage = driver.findElement(By.partialLinkText("下一页"));
            nextPageUrl = nextPage != null ? nextPage.getAttribute("href") : null;
        } while (nextPageUrl != null);
        chromeDriver.close();
    }

    private boolean openAndCopy(ChromeDriver chromeDriver, String href, YgdyEntity entity) {
        chromeDriver.get(href);
        try {
            String remark = chromeDriver.findElement(By.id("Zoom")).getText();
            entity.setRemark(remark);
        } catch (Exception e) {

        }

        try {
            WebElement downloadLink = chromeDriver.findElement(By.xpath("//font[@color='#0000ff']"));
            Actions actions = new Actions(chromeDriver);
            actions.contextClick(downloadLink).perform();
        } catch (Exception e) {
            try {
                WebElement downloadLink = chromeDriver.findElement(By.xpath("//td[@bgcolor='#fdfddf']"));
                Actions actions = new Actions(chromeDriver);
                actions.contextClick(downloadLink).perform();
            } catch (Exception ee) {
                return false;
            }
        }

        // 右键选择复制下载链接
        Robot robot = null;
        try {
            robot = new Robot();
            Thread.sleep(TIME_RIGHTCLICK_MOVE);
            robot.keyPress(KeyEvent.VK_DOWN);
            Thread.sleep(TIME_RIGHTCLICK_MOVE);
            robot.keyPress(KeyEvent.VK_DOWN);
            Thread.sleep(TIME_RIGHTCLICK_MOVE);
            robot.keyPress(KeyEvent.VK_DOWN);
            Thread.sleep(TIME_RIGHTCLICK_MOVE);
            robot.keyPress(KeyEvent.VK_DOWN);
            Thread.sleep(TIME_RIGHTCLICK_MOVE);
            robot.keyPress(KeyEvent.VK_ENTER);
            Thread.sleep(TIME_RIGHTCLICK);
        } catch (AWTException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 获取剪贴板
        String downloadUrl = WinSysUtils.getClipboardString();
        entity.setLink(downloadUrl);
        return true;
    }
}
