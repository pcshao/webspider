package cn.pcshao.spider.webspider;

import cn.pcshao.jdbctemplate.util.JdbcTemplate;
import cn.pcshao.spider.webspider.core.Ygdy8;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Application {

    public static void main(String[] args) {
        // http://chromedriver.storage.googleapis.com/index.html 寻找对应chrome驱动版本
        System.setProperty("webdriver.chrome.driver","D:\\IDEAworkspace\\webspider\\src\\main\\resources\\chromedriver.exe");
        ChromeDriver chromeDriver = new ChromeDriver();
        new Ygdy8().execute(chromeDriver);
        chromeDriver.close();
    }

    /**
     * TEST
     * @param args
     */
    /*
    public static void main(String[] args) {
        List<Map<String, Object>> list = new JdbcTemplate().queryList("select title from ygdy8 where 1 = ?", 1);
        for (Map<String, Object> record : list) {
            String title = record.get("title").toString();
            String encode = cnToEncode(title);
            new JdbcTemplate().update("update ygdy8 set post_name = ? where title = ?",
                    encode, title);
        }
    }

    private static String cnToEncode(String s ){
        char[] ch = s.toCharArray();
        String result = "";
        for(int i=0;i<ch.length;i++){
            char temp = ch[i];
            if(isChinese(temp)){
                try {
                    String encode = URLEncoder.encode(String.valueOf(temp), "utf-8");
                    result = result + encode;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }else{
                result = result+temp;
            }
        }
        return result;
    }

    private static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION) {
            return true;
        }
        return false;
    }*/
}
