package cn.pcshao.spider.webspider.dao.impl;

import cn.pcshao.jdbctemplate.util.JdbcTemplate;
import cn.pcshao.spider.webspider.dao.BaseDao;
import cn.pcshao.spider.webspider.entity.YgdyEntity;

import java.util.HashMap;

/**
 * @author pcshao.cn
 * @date 2021/4/4
 **/
public class NormalDao implements BaseDao<YgdyEntity> {

    private HashMap<String, YgdyEntity> datas;

    public void add(YgdyEntity entity) {
        // if (datas == null)
        //     datas = new HashMap<String, BaseEntity>();
        // datas.put(entity.getTitle(), entity);
        try {
            new JdbcTemplate().update("insert into ygdy8 (title, link, remark, page_link) values (?, ?, ?, ?)",
                    entity.getTitle(), entity.getLink(), entity.getRemark(), entity.getPageLink());
        } catch (Exception e) {

        }
    }

    public void update(YgdyEntity entity) {
        try {
            new JdbcTemplate().update("update ygdy8 set page_link = ? where title = ?",
                    entity.getPageLink(), entity.getTitle());
        } catch (Exception e) {

        }

    }
}

