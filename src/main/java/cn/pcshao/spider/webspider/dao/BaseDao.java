package cn.pcshao.spider.webspider.dao;

public interface BaseDao<T> {

    void add(T entity);

    void update(T entity);
}
