package cn.pcshao.spider.webspider.entity;

/**
 * @author pcshao.cn
 * @date 2021/4/5
 **/
public class YgdyEntity extends BaseEntity {

    private String pageLink;

    public YgdyEntity(String title) {
        super(title);
    }

    public String getPageLink() {
        return pageLink;
    }

    public void setPageLink(String pageLink) {
        this.pageLink = pageLink;
    }
}
