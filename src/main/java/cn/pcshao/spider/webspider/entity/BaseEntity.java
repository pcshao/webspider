package cn.pcshao.spider.webspider.entity;

public class BaseEntity {

    private String title;
    private String link;
    private String remark;

    public BaseEntity(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "title='" + title + '\'' +
                ", link='" + link + '\'' +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
